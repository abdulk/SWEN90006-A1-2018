package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.
  @Test public void aTest()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    final int expected = 2;
    final int actual = 1 + 1;
    assertEquals(expected, actual);
  }

  @Test public void B1_1()
  {
    
    // TEST FOR #  OF INSTRUCTION 1 WITH RET. 
    Machine m = new Machine();
    final List<String> lines1= new ArrayList<>();
            lines1.add("RET R1");
          assertEquals( m.execute(lines1), 0);
  }
  
  @Test public void B1_2()
  {
    
    // TEST FOR #  OF INSTRUCTION >1 WITH RET. 
    Machine m = new Machine();
    final List<String> lines1= new ArrayList<>();
    lines1.add("MOV R1 2");
            lines1.add("RET R1");
          assertEquals( m.execute(lines1), 2);
  }
  
  
  @Test(expected = NoReturnValueException.class) 
  public void B1_3()
  throws Throwable
{
	  // TEST FOR #  OF INSTRUCTION 1 WITH NO RET. 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("ADD R1 R2 R3");
	  m.execute(lines1);
  
}
  
  @Test public void B2_1()
 
{
	  // TEST FOR #  OF INSTRUCTION >1 WITH ADD AND REGISTER VALUE VALID . 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R2 2");
	  lines1.add("MOV R3 5");
	  lines1.add("ADD R1 R2 R3");
	 
	  
	  lines1.add("RET R1");
	  
	  
	  assertEquals( m.execute(lines1), 7);
  
}
  
  @Test public void B2_2()
  
  {
  	  // TEST FOR #  OF INSTRUCTION >1 WITH SUB AND REGISTER VALUE VALID .  
  	  Machine m = new Machine();
  	  final List<String> lines1= new ArrayList<>();
  	  lines1.add("MOV R2 2");
	  lines1.add("MOV R3 5");
  	  lines1.add("SUB R1 R2 R3");
  	  lines1.add("RET R1");
  	  
  	  
  	  assertEquals( m.execute(lines1), -3);
    
  }
  
  
  @Test public void B2_3()
  
  {
  	  // TEST FOR #  OF INSTRUCTION >1 WITH MUL AND REGISTER VALUE VALID .  
  	  Machine m = new Machine();
  	  final List<String> lines1= new ArrayList<>();
  	  lines1.add("MOV R2 2");
	  lines1.add("MOV R3 5");
  	  lines1.add("MUL R1 R2 R3");
  	  lines1.add("RET R1");
  	  
  	  
  	  assertEquals( m.execute(lines1), 10);
    
  }
  
 @Test public void B2_4()
  
  {
  	  // TEST FOR #  OF INSTRUCTION >1 WITH div AND REGISTER VALUE VALID .  
  	  Machine m = new Machine();
  	  final List<String> lines1= new ArrayList<>();
  	  lines1.add("MOV R2 10");
	  lines1.add("MOV R3 5");
  	  lines1.add("DIV R1 R2 R3");
  	  lines1.add("RET R1");
  	  
  	  
  	  assertEquals( m.execute(lines1), 2);
    
  }
 
 @Test(expected = InvalidInstructionException.class) 
 public void B2_5()
 throws Throwable
 
 {
 	  // TEST FOR #  OF INSTRUCTION >1 WITH  in valid insrtuction
 	  Machine m = new Machine();
 	  final List<String> lines1= new ArrayList<>();
 	  lines1.add("MOVED R2 2");
	  lines1.add("MOV R3 5");
 	  lines1.add("MUL R1 R2 R3");
 	  lines1.add("RET R1");
 	  
 	  
 	  m.execute(lines1);
   
 }
  
 @Test(expected = InvalidInstructionException.class) 
 public void B3_1()
 throws Throwable
 
 {
 	  // TEST FOR #  OF INSTRUCTION >1 WITH ADD AND REGISTER VALUE INVALID .  
 	  Machine m = new Machine();
 	  final List<String> lines1= new ArrayList<>();
 	  lines1.add("ADD R-1 R2 R3");
	  
 	  lines1.add("RET R1");
 	  
 	  
 	  m.execute(lines1);
   
 }
 
 @Test(expected = InvalidInstructionException.class) 
 public void B3_2()
 throws Throwable
 
 {
 	  // TEST FOR #  OF INSTRUCTION >1 WITH ADD AND REGISTER VALUE INVALID .  
 	  Machine m = new Machine();
 	  final List<String> lines1= new ArrayList<>();
 	 lines1.add("ADD R32 R2 R3");
	  
	  lines1.add("RET R1");
 	  
 	  
 	  m.execute(lines1);
   
 }
 
  
 @Test public void B3_3()
 
 
 {
 	  // TEST FOR #  OF INSTRUCTION >1 WITH ADD AND REGISTER VALUE VALID .  
 	  Machine m = new Machine();
 	  final List<String> lines1= new ArrayList<>();
 	 lines1.add("ADD R0 R2 R3");
	  
	  lines1.add("RET R0");
 	  
 	 assertEquals( m.execute(lines1), 0);
 	 
 }
 
@Test public void B3_4()
 
 
 {
 	  // TEST FOR #  OF INSTRUCTION >1 WITH ADD AND REGISTER VALUE VALID .  
 	  Machine m = new Machine();
 	  final List<String> lines1= new ArrayList<>();
 	 lines1.add("ADD R31 R2 R3");
	  
	  lines1.add("RET R31");
 	  
 	 assertEquals( m.execute(lines1), 0);
 	 
 }
 
 
@Test(expected = InvalidInstructionException.class) 
public void B4_1()
throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH MOV AND REGISTER VALUE INVALID AND VAL VALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R32 34");
	  m.execute(lines1);

}


@Test public void B4_2()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH MOV AND REGISTER VALUE VALID AND VAL VALID ON POINT FOR VAL
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R31 -65535");
	  lines1.add("RET R31");
	  assertEquals( m.execute(lines1), -65535);

}

@Test public void B4_3()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH MOV AND REGISTER VALUE VALID AND VAL VALID ON POINT FOR VAL
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R31 65535");
	  lines1.add("RET R31");
	  assertEquals( m.execute(lines1), 65535);

}
		  
  
@Test(expected = InvalidInstructionException.class) 
public void B4_4()
throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH MOV AND REGISTER VALUE inVALID AND VAL IN VALID OFF POINT FOR VAL
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R31 -65536");
	  lines1.add("RET R31");
	  m.execute(lines1);

}
@Test(expected = InvalidInstructionException.class) 
public void B4_5()
throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH MOV AND REGISTER VALUE inVALID AND VAL IN VALID OFF POINT FOR VAL
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R31 65536");
	  lines1.add("RET R31");
	  m.execute(lines1);

}



@Test(expected = InvalidInstructionException.class) 
public void B5_1()
throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE INVALID OFF POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("STR R32 R45 -655");
	  lines1.add("RET R31");
	  m.execute(lines1);

}


@Test(expected = InvalidInstructionException.class) 
public void B5_2()
throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL INVALID OFF POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("STR R31 R4 -65536");
	  lines1.add("RET R31");
	  m.execute(lines1);

}

@Test(expected = InvalidInstructionException.class) 
public void B5_3()
throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL INVALID OFF POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("STR R31 R4 65536");
	  lines1.add("RET R31");
	  m.execute(lines1);

}


@Test public void B5_4()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL VALID;
	  //TESTING LDR FUNCTIONALITY & b+v inside the given range ON POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R1 5");
	  lines1.add("MOV R4 65534");
	  lines1.add("STR R4 1 R1");
	  lines1.add("LDR R2 R4 1");
	  
	  lines1.add("RET R2");
	  assertEquals( m.execute(lines1), 5);
	

}

@Test public void B5_5()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL VALID;
	  //TESTING LDR FUNCTIONALITY & b+v inside the given range ON POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R1 5");
	  lines1.add("MOV R4 2");
	  lines1.add("STR R4 -2 R1");
	  lines1.add("LDR R2 R4 -2");
	  
	  lines1.add("RET R2");
	  assertEquals( m.execute(lines1), 5);
	

}


@Test public void B5_6()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL VALID
	 //TESTING LDR FUNCTIONALITY & b+v  OUT SIDE the given range OFF POINT
	 Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R1 5");
	  lines1.add("MOV R4 65535");
	  lines1.add("STR R4 1 R1");
	  lines1.add("LDR R2 R4 1");
	  
	  lines1.add("RET R2");
	  assertEquals( m.execute(lines1), 0);
	  

}



@Test public void B5_7()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL VALID
		 //TESTING LDR FUNCTIONALITY & b+v  OUT SIDE the given range OFF POINT
	 Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R1 5");
	  lines1.add("MOV R4 -2");
	  lines1.add("STR R4 1 R1");
	  lines1.add("LDR R2 R4 1");
	  
	  lines1.add("RET R2");
	  assertEquals( m.execute(lines1), 0);

}

@Test(timeout = 5000) 
 public void b6_1()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JMP vAL=1  ON POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 1");
	  lines1.add(" JMP 1");
	  lines1.add(" MOV R1 2");
	 
	  
	  
	  lines1.add("RET R1");
	  assertEquals( m.execute(lines1), 2);
	
  
}


@Test(timeout = 5000) 
public void b6_8()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JMP vAL=O ON POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 1");
	  lines1.add(" JMP 0");
	  lines1.add(" MOV R1 2");
	 
	  
	  
	  lines1.add("RET R1");
	  
	
 
}

@Test(timeout = 5000) 
 public void b6_2()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JMP vAL=1
	
		 //TESTING  FUNCTIONALITY & VAL + PC  IN SIDE the given range ON POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 1");
	  lines1.add(" JMP 2");
	  lines1.add(" MOV R1 3");
	  lines1.add(" MOV R1 2");
	 
	  
	  
	  lines1.add("RET R1");
	  assertEquals( m.execute(lines1), 2);
	
  
}


@Test(timeout = 5000) 
 public void b6_3()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JMP AND VAL +PC  IS WITH IN THE RANGE
	//TESTING  FUNCTIONALITY & VAL + PC  IN SIDE the given range ON POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 1");
	  lines1.add(" MOV R2 2");
	  lines1.add(" JMP 2");
	  lines1.add("MOV R2 5");
	  lines1.add("MOV R4 8");
	  
	 
	  
	  
	  lines1.add("RET R4");
	  assertEquals( m.execute(lines1), 8);
	
  
}

@Test(timeout = 5000, expected = NoReturnValueException.class) 
public void B6_4()
throws Throwable
{
	//TESTING  FUNCTIONALITY & VAL + PC  OUT SIDE the given range OFF POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  
	  lines1.add(" MOV R1 1");
	  lines1.add(" MOV R2 2");
	  lines1.add(" JMP -3");
	  lines1.add("RET R4");
	  
	  m.execute(lines1);

}

@Test(timeout = 5000, expected = NoReturnValueException.class) 
public void B6_5()
throws Throwable
{
	//TESTING  FUNCTIONALITY & VAL + PC  OUT SIDE the given range OFF POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  
	  
	  
	  lines1.add(" MOV R4 2");
	  lines1.add(" JMP 5");
	  lines1.add(" MOV R5 2");
	  lines1.add(" MOV R6 2");
	  lines1.add(" MOV R7 2");
	  lines1.add("RET R4");
	  
	  m.execute(lines1);

}

@Test(timeout = 5000, expected = InvalidInstructionException.class) 
public void B7_1()
throws Throwable
{

	// TEST FOR #  OF INSTRUCTION >1 WITH REGISTER VAL IN VALID OFF POINT
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	 
	  lines1.add(" JZ R32 1");
	  lines1.add(" MOV R1 2");
	 
	  
	  
	  lines1.add("RET R34");
	  m.execute(lines1) ;
	
  
}






@Test (timeout = 5000)
public void b7_3()

{
	// TEST FOR REGISTER 0 FOR JZ
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 0");
	  lines1.add(" MOV R2 2");
	  lines1.add(" JZ  R1 2");
	  lines1.add(" MOV R1 2");
	  lines1.add(" MOV R2 3");
	  
	  
	  lines1.add("RET R1");
	  lines1.add("RET R2");
	  assertEquals( m.execute(lines1), 0);
	
	  
	
  
}

@Test(timeout = 5000)
public void b7_4()

{
	// TEST FOR REGISTER  != 0 FOR JZ
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	
	  lines1.add(" MOV R2 2");
	  lines1.add(" JZ  R2 2");
	  lines1.add(" MOV R1 1");
	  lines1.add(" MOV R2 3");
	  
	  
	  lines1.add("RET R1");
	  assertEquals( m.execute(lines1), 1);
	
	  
	
  
}





@Test(timeout = 5000, expected = NoReturnValueException.class) 
public void B7_5()
throws Throwable
{
	  // TEST FOR JZ  where pc is outside the given range .. off point
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  
	  lines1.add("MOV R1 0");
	  lines1.add(" MOV R2 2");
	  lines1.add(" JZ  R1 -3");
	  lines1.add("RET R4");
	  
	  m.execute(lines1);

}

@Test(timeout = 5000,expected = NoReturnValueException.class) 
public void B7_6()
throws Throwable
{
	 // TEST FOR JZ  where pc is outside the given range .. off point
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  
	  
	  
	  lines1.add(" MOV R1 0");
	  lines1.add(" JZ  R1 5");
	  lines1.add(" MOV R5 2");
	  lines1.add(" MOV R6 2");
	  lines1.add(" MOV R7 2");
	  lines1.add("RET R4");
	  
	  m.execute(lines1);

}


@Test(timeout = 5000,expected = NoReturnValueException.class) 
public void B8_1()
throws Throwable
{
	  // TEST FOR #  OF INSTRUCTION  WITH NO RET. 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  
	  
	  
	  lines1.add(" MOV R1 3");
	  lines1.add(" JZ  R1 2");
	  lines1.add(" MOV R5 2");
	  lines1.add(" MOV R6 2");
	  lines1.add(" MOV R7 2");
	 
	  
	  m.execute(lines1);

}


@Test(timeout = 5000) 
public void B8_2()
throws Throwable
{
	  // TEST FOR #  OF INSTRUCTION  WITH 1 RET. 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  
	  
	  
	  lines1.add(" MOV R1 1");
	  lines1.add(" JZ  R1 2");
	  lines1.add(" MOV R5 2");
	  lines1.add(" RET R4");
	 
	  assertEquals( m.execute(lines1), 0);
		
	 
	  
	  m.execute(lines1);

}

@Test(timeout = 5000) 
public void B8_3()
throws Throwable
{
	  // TEST FOR #  OF INSTRUCTION WITH 2 RET. 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  
	  
	  
	  lines1.add(" MOV R1 2");
	  lines1.add(" JZ  R1 2");
	 
	  lines1.add(" RET R4");
	  lines1.add(" RET R3");
	 
	  
	  assertEquals( m.execute(lines1), 0);

}

  

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
