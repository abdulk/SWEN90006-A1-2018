package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  @Test public void E1()
  {
    
    // TEST FOR #  OF INSTRUCTION 1 WITH RET. 
    Machine m = new Machine();
    final List<String> lines1= new ArrayList<>();
            lines1.add("RET R1");
          assertEquals( m.execute(lines1), 0);
  }
  
  
  
  
  
  @Test(expected = NoReturnValueException.class) 
  public void E2()
  throws Throwable
{
	  // TEST FOR #  OF INSTRUCTION 1 WITH NO RET. 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("ADD R1 R2 R3");
	  m.execute(lines1);
  
}
  
  @Test public void E3()
 
{
	  // TEST FOR #  OF INSTRUCTION >1 WITH ADD AND REGISTER VALUE VALID . 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R2 2");
	  lines1.add("MOV R3 5");
	  lines1.add("ADD R1 R2 R3");
	 
	  
	  lines1.add("RET R1");
	  
	  
	  assertEquals( m.execute(lines1), 7);
  
}
  
  @Test public void E6()
  
  {
  	  // TEST FOR #  OF INSTRUCTION >1 WITH SUB AND REGISTER VALUE VALID .  
  	  Machine m = new Machine();
  	  final List<String> lines1= new ArrayList<>();
  	  lines1.add("MOV R2 2");
	  lines1.add("MOV R3 5");
  	  lines1.add("SUB R1 R2 R3");
  	  lines1.add("RET R1");
  	  
  	  
  	  assertEquals( m.execute(lines1), -3);
    
  }
  
  
  @Test public void E7()
  
  {
  	  // TEST FOR #  OF INSTRUCTION >1 WITH MUL AND REGISTER VALUE VALID .  
  	  Machine m = new Machine();
  	  final List<String> lines1= new ArrayList<>();
  	  lines1.add("MOV R2 2");
	  lines1.add("MOV R3 5");
  	  lines1.add("MUL R1 R2 R3");
  	  lines1.add("RET R1");
  	  
  	  
  	  assertEquals( m.execute(lines1), 10);
    
  }
  
  @Test public void E8()
  
  {
  	  // TEST FOR #  OF INSTRUCTION >1 WITH DIV AND REGISTER VALUE VALID .  
  	  Machine m = new Machine();
  	  final List<String> lines1= new ArrayList<>();
  	  lines1.add("MOV R2 10");
	  lines1.add("MOV R3 5");
  	  lines1.add("DIV R1 R2 R3");
  	  lines1.add("RET R1");
  	  
  	  
  	  assertEquals( m.execute(lines1), 2);
    
  }
  
  
@Test public void E9()
  
  {
  	  // TEST FOR #  RET with value valid  
  	  Machine m = new Machine();
  	  final List<String> lines1= new ArrayList<>();
  	  lines1.add("MOV R2 10");
	  lines1.add("MOV R3 5");
  	  lines1.add("DIV R1 R2 R3");
  	  lines1.add("RET R1");
  	  
  	  
  	  assertEquals( m.execute(lines1), 2);
    
  }
  
  @Test(expected = InvalidInstructionException.class) 
  public void E4()
  throws Throwable
{
	  // TEST FOR #  OF INSTRUCTION >1 WITH add AND REGISTER VALUE inVALID  
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("ADD R33 R2 R3");
	  lines1.add("RET R3");
	  
	  m.execute(lines1);
  
}
  
  @Test(expected = InvalidInstructionException.class) 
  public void E5()
  throws Throwable
{
	  // TEST FOR #  OF INSTRUCTION >1 WITH add AND REGISTER VALUE inVALID  
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("ADD R-4 R2 R3");
	  lines1.add("RET R3");
	  
	  m.execute(lines1);
  
}
  
 
  @Test(expected = InvalidInstructionException.class) 
  public void E13()
  throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH MOV AND REGISTER VALUE inVALID 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R34 34");
	  m.execute(lines1);
  
}
  
  @Test(expected = InvalidInstructionException.class) 
  public void E14()
  throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH MOV AND REGISTER VALUE inVALID 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R-1 34");
	  m.execute(lines1);
  
}
  
  
  @Test public void E10()
 
{
	// TEST FOR #  OF INSTRUCTION >1 WITH MOV AND REGISTER VALUE VALID AND VAL VALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R31 34");
	  lines1.add("RET R31");
	  assertEquals( m.execute(lines1), 34);
  
}

  
  @Test(expected = InvalidInstructionException.class) 
  public void E11()
  throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH MOV AND REGISTER VALUE VALID AND VAL INVALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R31 65538");
	  lines1.add("RET R31");
	  m.execute(lines1);
  
}

  @Test(expected = InvalidInstructionException.class) 
  public void E12()
  throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH MOV AND REGISTER VALUE VALID AND VAL INVALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R31 -65538");
	  lines1.add("RET R31");
	  m.execute(lines1);
  
}

  @Test(expected = InvalidInstructionException.class) 
  public void E17()
  throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE INVALID 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("STR R31 R45 -655");
	  lines1.add("RET R31");
	  m.execute(lines1);
  
}
  
  @Test(expected = InvalidInstructionException.class) 
  public void E18()
  throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE INVALID 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("STR R-31 R45 -655");
	  lines1.add("RET R31");
	  m.execute(lines1);
  
}
  
  
  
  @Test(expected = InvalidInstructionException.class) 
  public void E19()
  throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL INVALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("STR R31 R4 -655444");
	  lines1.add("RET R31");
	  m.execute(lines1);
  
}
  
  
  @Test(expected = InvalidInstructionException.class) 
  public void E20()
  throws Throwable
{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL INVALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("STR R31 R4 655444");
	  lines1.add("RET R31");
	  m.execute(lines1);
  
}
  
 
  @Test public void E15()
  
{
	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL VALID;
	  //TESTING LDR FUNCTIONALITY & b+v inside the given range
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R1 5");
	  lines1.add("MOV R4 2");
	  lines1.add("STR R4 3 R1");
	  lines1.add("LDR R2 R4 3");
	  
	  lines1.add("RET R2");
	  assertEquals( m.execute(lines1), 5);
	
  
}
  
  @Test public void E16_1()
  
  {
  	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL VALID;
  	  //TESTING LDR FUNCTIONALITY & b+v less then 0
  	  Machine m = new Machine();
  	  final List<String> lines1= new ArrayList<>();
  	lines1.add("MOV R1 5");
  	  lines1.add("MOV R4 -7");
  	  lines1.add("STR R4 3 R1");
  	  lines1.add("LDR R2 R4 3");
  	  
  	  lines1.add("RET R2");
  	  assertEquals( m.execute(lines1), 0);
  	
    
  }
  
@Test public void E16_2()
  
  {
  	// TEST FOR #  OF INSTRUCTION >1 WITH STR AND REGISTER VALUE VALID AND VAL VALID;
  	  //TESTING LDR FUNCTIONALITY & b+v greater then 65535
  	  Machine m = new Machine();
  	  final List<String> lines1= new ArrayList<>();
  	  lines1.add("MOV R1 5");
  	  lines1.add("MOV R4 65535");
  	  lines1.add("STR R4 78 R1");
  	  lines1.add("LDR R2 R4 78");
  	  
  	  lines1.add("RET R2");
  	  assertEquals( m.execute(lines1), 0);
  	
    
  }
  
 






@Test public void E21()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH LDR AND REGISTER VALUE VALID AND VAL VALID;
	  //TESTING LDR FUNCTIONALITY & b+v inside the given range
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add("MOV R1 5");
	  lines1.add("MOV R4 2");
	  lines1.add("STR R4 3 R1");
	  lines1.add("LDR R2 R4 3");
	  
	  lines1.add("RET R2");
	  assertEquals( m.execute(lines1), 5);
	

}




@Test (timeout = 5000)
 public void E23()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JMP vAL=1
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 1");
	  lines1.add(" JMP 1");
	  lines1.add(" MOV R1 2");
	 
	  
	  
	  lines1.add("RET R1");
	  assertEquals( m.execute(lines1), 2);
	
  
}
@Test (timeout = 5000)
 public void E22()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JMP vAL=0
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 1");
	  lines1.add(" JMP 0");
	  lines1.add(" MOV R1 2");
	 
	  
	  
	  lines1.add("RET R1");
	
	
  
}

@Test (timeout = 5000)
public void E24()

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JMP AND VAL +PC  IS WITH IN THE RANGE
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 1");
	  lines1.add(" JMP 2");
	  lines1.add(" MOV R1 3");
	  lines1.add(" MOV R2 5");
	  
	 
	  
	  
	  lines1.add("RET R1");
	  assertEquals( m.execute(lines1), 1);
	
  
}

@Test(timeout = 5000, expected = NoReturnValueException.class) 
public void E25()
throws Throwable

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JMP AND VAL +PC  IS LESS THEN 0
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 1");
	  lines1.add(" JMP -3");
	  lines1.add("RET R1");
	  m.execute(lines1);
	 
	
  
}

@Test(timeout = 5000,expected = NoReturnValueException.class) 
public void E26()
throws Throwable

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JMP AND VAL +PC  IS GRETER THEN N-1
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 1");
	  lines1.add(" JMP 79");
	  lines1.add("RET R1");
	  m.execute(lines1);
	 
	
  
}

@Test(timeout = 5000,expected = InvalidInstructionException.class) 
public void E27()
throws Throwable

{
	// TEST FOR #  OF INSTRUCTION >1 WITH  JMP AND VAL IN VALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" JMP 7900000");
	  lines1.add("RET R1");
	  m.execute(lines1);
	 
	
  
}


@Test(timeout = 5000, expected = InvalidInstructionException.class) 
public void E28()
throws Throwable

{
	// TEST FOR #  OF INSTRUCTION >1 WITH  JMP AND VAL IN VALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" JMP -678888");
	  lines1.add("RET R1");
	  m.execute(lines1);
	 
	
  
}


@Test(timeout = 5000, expected = InvalidInstructionException.class) 
public void E29()
throws Throwable

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JZ AND  REGISTER IN VALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" JZ R56 2");
	  lines1.add("RET R1");
	  m.execute(lines1);
	 
	
  
}

@Test(timeout = 5000,expected = InvalidInstructionException.class) 
public void E30()
throws Throwable

{
	// TEST FOR #  OF INSTRUCTION >1 WITH JZ AND  REGISTER IN VALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" JZ R-4 2");
	  lines1.add("RET R1");
	  m.execute(lines1);
	 
	
  
}
 
@Test(timeout = 5000,expected = InvalidInstructionException.class) 
public void E31()
throws Throwable

{
	// TEST FOR #  OF INSTRUCTION >1 WITH  JMP AND VAL IN VALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" JZ R5 2899898");
	  lines1.add("RET R1");
	  m.execute(lines1);
	 
	
  
}
  

@Test(timeout = 5000,expected = InvalidInstructionException.class) 
public void E32()
throws Throwable

{
	// TEST FOR #  OF INSTRUCTION >1 WITH  JMP AND VAL IN VALID
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" JZ R5 -2899898");
	  lines1.add("RET R1");
	  m.execute(lines1);
	 
	
  
}
  

@Test (timeout = 5000)
public void E34()


{
	// TEST FOR #  OF INSTRUCTION >1 WITH  JMP  AND VAL VALID AND R1=0 AND VAL +PC WITH IN RANGE AND RET =1
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 0");
	  lines1.add(" JZ R1 2");
	  lines1.add(" MOV R1 3");
	  lines1.add(" RET R1");
	 
	  
	  
	 
	  assertEquals(m.execute(lines1),0);
	 
}
@Test (timeout = 5000)
public void E33()


{
	// TEST FOR #  OF INSTRUCTION >1 WITH  JMP  AND VAL VALID AND R1!=0 
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 1");
	  lines1.add(" JZ R1 2");
	  lines1.add(" MOV R1 3");
	  lines1.add(" RET R1");
	 
	  
	  
	 
	  assertEquals(m.execute(lines1),3);
	 
	
  
}
  
@Test(timeout = 5000, expected = NoReturnValueException.class) 
public void E35()
throws Throwable


{
	// TEST FOR #  OF INSTRUCTION >1 WITH  JMP  AND VAL VALID AND R1=0 AND VAL +PC <0
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 0");
	  lines1.add(" JZ R1 -3");
	  lines1.add(" MOV R1 3");
	  lines1.add(" RET R1");
	 
	  
	  
	 
	 m.execute(lines1);
	 
	
  
}
@Test(timeout = 5000, expected = NoReturnValueException.class) 
public void E36()
throws Throwable


{
	// TEST FOR #  OF INSTRUCTION >1 WITH  JMP  AND VAL VALID AND R1=0 AND VAL +PC >0
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOV R1 0");
	  lines1.add(" JZ R1 78");
	  lines1.add(" MOV R1 3");
	  lines1.add(" RET R1");
	 
	  
	  
	 
	 m.execute(lines1);
}
   
@Test(expected = InvalidInstructionException.class) 
public void E37()
throws Throwable


{
	// TEST FOR   INVALID INSTRUCTION
	  Machine m = new Machine();
	  final List<String> lines1= new ArrayList<>();
	  lines1.add(" MOVTY R1 0");
	  lines1.add(" RET R1");
	 
	  
	  
	 
	 m.execute(lines1);
}


@Test (timeout = 5000)

public void E38()


{
	
	// TEST FOR #  OF INSTRUCTION >1 WITH  JMP  AND VAL VALID AND R1=0 AND VAL +PC WITH IN RANGE AND RET >1
		  Machine m = new Machine();
		  final List<String> lines1= new ArrayList<>();
		  lines1.add(" MOV R1 0");
		  lines1.add(" JZ R1 2");
		  lines1.add(" MOV R2 3");
		  lines1.add(" MOV R4 3");
		  lines1.add(" RET R2");
		  lines1.add(" RET R2");
	 
	  
	  
	 
	  assertEquals(m.execute(lines1),0);
	 
	
  
}
  
@Test( timeout = 5000 ,expected = NoReturnValueException.class) 
public void E39()
throws Throwable

{
	
	// TEST FOR #  OF INSTRUCTION >1 WITH  JMP  AND VAL VALID AND R1=0 AND VAL +PC WITH IN RANGE AND RET =0
		  Machine m = new Machine();
		  final List<String> lines1= new ArrayList<>();
		  lines1.add(" MOV R1 0");
		  lines1.add(" JZ R1 5");
		  lines1.add(" MOV R1 3");
		 
		  
	 
	  
	  
	 
	  m.execute(lines1);
	 
	
  
}
  
  
  

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
 

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
